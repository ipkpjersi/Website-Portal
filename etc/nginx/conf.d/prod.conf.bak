# HTTP
server {
    listen                      80;
    listen                      [::]:80;
    server_name					rsc.vet game.openrsc.com;

    root 						/var/www/html/portal/public;
    index    					index.html index.htm index.php;

    # PHP proxy
    location ~ \.php$ {
                try_files                         $fastcgi_script_name =404;
                fastcgi_split_path_info           ^(.+\.php)(/.+)$;
                fastcgi_pass                      php:9000;
                fastcgi_index                     index.php;
                set            $path_info         $fastcgi_path_info;
                fastcgi_param  PATH_INFO          $path_info;
                include                           fastcgi_params;
                fastcgi_intercept_errors          on;
                fastcgi_read_timeout              600;
                fastcgi_buffering                 off;
                fastcgi_param  SCRIPT_FILENAME    $document_root$fastcgi_script_name;
                fastcgi_param  QUERY_STRING       $query_string;
                fastcgi_param  REQUEST_METHOD     $request_method;
                fastcgi_param  CONTENT_TYPE       $content_type;
                fastcgi_param  CONTENT_LENGTH     $content_length;
                fastcgi_param  SCRIPT_NAME        $fastcgi_script_name;
                fastcgi_param  REQUEST_URI        $request_uri;
                fastcgi_param  DOCUMENT_URI       $document_uri;
                fastcgi_param  DOCUMENT_ROOT      $document_root;
                fastcgi_param  SERVER_PROTOCOL    $server_protocol;
                fastcgi_param  REQUEST_SCHEME     $scheme;
                fastcgi_param  HTTPS              $https if_not_empty;
                fastcgi_param  GATEWAY_INTERFACE  CGI/1.1;
                fastcgi_param  SERVER_SOFTWARE    nginx;
                fastcgi_param  REMOTE_ADDR        $remote_addr;
                fastcgi_param  REMOTE_PORT        $remote_port;
                fastcgi_param  SERVER_ADDR        $server_addr;
                fastcgi_param  SERVER_PORT        $server_port;
                fastcgi_param  SERVER_NAME        $server_name;
                fastcgi_param  REDIRECT_STATUS    200;
    }
    # General
    location / {
        try_files               $uri $uri/ /index.php?$query_string;
    }
    # Wiki proxy
    location /wiki {
            proxy_pass                                 http://mediawiki:9090;
            proxy_set_header                           X-Real-IP $remote_addr;
            proxy_set_header                           X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header                           X-Forwarded-Proto $scheme;
            proxy_cache_bypass                         $http_upgrade;
    }
    # PHPBB
    location /board {
        try_files               $uri $uri/ @rewriteapp @extensionless-php;
    }
    location /board/install/ {
        try_files               $uri $uri/ @rewrite_installapp;
    }
    location @extensionless-php {
        rewrite                 ^(.*)$ $1.php last;
    }
    location @rewriteapp {
        rewrite                 ^(.*)$ /app.php/$1 last;
    }
    location @rewrite_installapp {
        try_files               $uri $uri/ /board/install/app.php?$query_string;
    }

    # Deny access to version control system directories.
    location ~ /\.svn|/\.git {
        deny                    all;
        internal;
    }
    location ~ /\.(?!well-known).* {
        deny                    all;
    }
    # Deny access to internal phpbb files.
    location ~ /(config\.php|common\.php|board/includes|board/cache|board/files|board/store|board/images/avatars/upload) {
        deny                    all;
        # deny was ignored before 0.8.40 for connections over IPv6.
        # Use internal directive to prohibit access on older versions.
        internal;
    }
    # Dont fill up the access log
    location ~ /\. {
        deny 					all;
        access_log 				off;
        log_not_found 			off;
    }
    location = /robots.txt {
        access_log 				off;
        log_not_found 			off;
    }
    location = /favicon.ico {
        access_log 				off;
        log_not_found 			off;
    }
    # Deny odd HTTP requests
    if ($request_method !~ ^(GET|HEAD|POST)$ ) {
        return 444; }
}

# HTTPS
server {
    listen                      443 ssl;
    server_name					rsc.vet game.openrsc.com;

    ssl_certificate             /etc/nginx/ssl/cloudflare.pem;
    ssl_certificate_key         /etc/nginx/ssl/cloudflare.key;
    ssl_protocols               TLSv1.2 TLSv1.3;
    ssl_prefer_server_ciphers   on;

    root 						/var/www/html/portal/public;
    index    					index.html index.htm index.php;

    # PHP proxy
    location ~ \.php$ {
                try_files                         $fastcgi_script_name =404;
                fastcgi_split_path_info           ^(.+\.php)(/.+)$;
                fastcgi_pass                      php:9000;
                fastcgi_index                     index.php;
                set            $path_info         $fastcgi_path_info;
                fastcgi_param  PATH_INFO          $path_info;
                include                           fastcgi_params;
                fastcgi_intercept_errors          on;
                fastcgi_read_timeout              600;
                fastcgi_buffering                 off;
                fastcgi_param  SCRIPT_FILENAME    $document_root$fastcgi_script_name;
                fastcgi_param  QUERY_STRING       $query_string;
                fastcgi_param  REQUEST_METHOD     $request_method;
                fastcgi_param  CONTENT_TYPE       $content_type;
                fastcgi_param  CONTENT_LENGTH     $content_length;
                fastcgi_param  SCRIPT_NAME        $fastcgi_script_name;
                fastcgi_param  REQUEST_URI        $request_uri;
                fastcgi_param  DOCUMENT_URI       $document_uri;
                fastcgi_param  DOCUMENT_ROOT      $document_root;
                fastcgi_param  SERVER_PROTOCOL    $server_protocol;
                fastcgi_param  REQUEST_SCHEME     $scheme;
                fastcgi_param  HTTPS              $https if_not_empty;
                fastcgi_param  GATEWAY_INTERFACE  CGI/1.1;
                fastcgi_param  SERVER_SOFTWARE    nginx;
                fastcgi_param  REMOTE_ADDR        $remote_addr;
                fastcgi_param  REMOTE_PORT        $remote_port;
                fastcgi_param  SERVER_ADDR        $server_addr;
                fastcgi_param  SERVER_PORT        $server_port;
                fastcgi_param  SERVER_NAME        $server_name;
                fastcgi_param  REDIRECT_STATUS    200;
    }
    # General
    location / {
        try_files               $uri $uri/ /index.php?$query_string;
    }
    # Wiki proxy
    location /wiki {
            proxy_pass                                 http://mediawiki:9090;
            proxy_set_header                           X-Real-IP $remote_addr;
            proxy_set_header                           X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header                           X-Forwarded-Proto $scheme;
            proxy_cache_bypass                         $http_upgrade;
    }
    # PHPBB
    location /board {
        try_files               $uri $uri/ @rewriteapp @extensionless-php;
    }
    location /board/install/ {
        try_files               $uri $uri/ @rewrite_installapp;
    }
    location @extensionless-php {
        rewrite                 ^(.*)$ $1.php last;
    }
    location @rewriteapp {
        rewrite                 ^(.*)$ /app.php/$1 last;
    }
    location @rewrite_installapp {
        try_files               $uri $uri/ /board/install/app.php?$query_string;
    }

    # Deny access to version control system directories.
    location ~ /\.svn|/\.git {
        deny                    all;
        internal;
    }
    location ~ /\.(?!well-known).* {
        deny                    all;
    }
    # Deny access to internal phpbb files.
    location ~ /(config\.php|common\.php|board/includes|board/cache|board/files|board/store|board/images/avatars/upload) {
        deny                    all;
        # deny was ignored before 0.8.40 for connections over IPv6.
        # Use internal directive to prohibit access on older versions.
        internal;
    }
    # Dont fill up the access log
    location ~ /\. {
        deny 					all;
        access_log 				off;
        log_not_found 			off;
    }
    location = /robots.txt {
        access_log 				off;
        log_not_found 			off;
    }
    location = /favicon.ico {
        access_log 				off;
        log_not_found 			off;
    }
    # Deny odd HTTP requests
    if ($request_method !~ ^(GET|HEAD|POST)$ ) {
        return 444; }
}